/**
 * External Dependencies
 *
 * @format
 */
import React, { Component } from 'react';

/**
 * Internal Dependencies
 */
import { UserContext } from 'Context';

export default class User extends Component {
	render() {
		return (
			<UserContext.Consumer>
				{ context => (
					<div>
						<h1>User Info</h1>
						<h3>{ context.user.name }</h3>
						<h3>{ context.user.email }</h3>
						<button onClick={ context.logout }>Logout</button>
					</div>
				) }
			</UserContext.Consumer>
		);
	}
}
