/** @format */

// export * from './Filename`; - If we were to have multiple utilities in a single file
import Portal from './Portal';
import Toggle from './Toggle';
import elevation from './elevation';
import transition from './transition';
import colors from './colors'; // object containing all of the colors

// The * exports are because we are not targeting the default exports, rather all of the non-default exports in that file.
export * from './colors';
export * from './position';

export { Portal, Toggle, elevation, transition, colors };
