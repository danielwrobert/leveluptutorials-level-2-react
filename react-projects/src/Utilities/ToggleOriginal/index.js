/**
 * External Dependencies
 *
 * @format
 */
import React, { Component } from 'react';

export default class Toggle extends Component {
	state = {
		on: false,
	};

	toggle = () => {
		this.setState( {
			on: ! this.state.on,
		} );
	};

	render() {
		return (
			<div>
				{ /* { this.state.on && <h1>Toggle me!</h1> --> Will only output element if `on` is `true` } */ }
				{ this.state.on &&
					this.props
						.children /* Will only output element (this.props.children) if `on` is `true` */ }
				<button onClick={ this.toggle }>Show/Hide</button>
			</div>
		);
	}
}
