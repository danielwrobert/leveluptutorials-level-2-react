/**
 * External Dependencies
 *
 * @format
 */
import React, { Component, Fragment } from 'react';

/**
 * Internal Dependencies
 */
import logo from './logo.svg';
import { Toggle } from 'Utilities';
import { Modal, Drag } from 'Elements';
import { UserProvider } from 'Context';

/**
 * Styles
 */
import './App.css';

class App extends Component {
	render() {
		return (
			<UserProvider>
				<div className="App">
					<header className="App-header">
						<img src={ logo } className="App-logo" alt="logo" />
						<h1 className="App-title">Welcome to React</h1>
					</header>
					<Toggle>
						{ ( { on, toggle } ) => (
							// above, we destructure `on` and `toggle` from the object passed in
							<Fragment>
								<button onClick={ toggle }>Login</button>
								<Modal on={ on } toggle={ toggle }>
									<h1>Bonjour! I am in ze modal! 🇫🇷</h1>
								</Modal>
							</Fragment>
						) }
					</Toggle>
					<Drag />
				</div>
			</UserProvider>
		);
	}
}

export default App;
