/** @format */
import { UserContext } from './UserContext'; // Not a default export so need to import brackets
import UserProvider from './UserProvider'; // Default export so can import via name

export { UserContext, UserProvider };

/**
 * Note: The above components need to be exported as `default` in their respective files, or they will not work.
 * Alternatively, you can export non-default exports as follows:
 *
 * export * from './colors';
 * export * from './position';
 */
