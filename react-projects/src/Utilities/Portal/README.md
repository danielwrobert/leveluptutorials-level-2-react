## Lesson 5: Creating A Re-usable Portal

Portals provide a first-class way to render children into a DOM node that exists outside the DOM hierarchy of the parent component.

[React Docs](https://reactjs.org/docs/portals.html#___gatsby)