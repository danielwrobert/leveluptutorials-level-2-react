## Lesson 2: Understanding Render Props

We are building off of the previous lesson but now we will make the `Toggle` component more flexible using Render Props.

The naming might sound confusing but what it means is passing in a property on your component to render something. The actual prop itself could be named anything, it doesn't _have_ to be named "render". It's typically used as "render" however, because that's what you're doing with it - rendering something.

We pass a render prop an arrow function (or a function) with what we want to output with our component - very much like a Stateless Functional Component:

```
const Hello = () => (

);
```

The only real difference with a Render Prop is that we're not assigning it to a variable name, we're just passing it in as an anonymous function.

We then want to use this prop in the component. Once we destructure `render` from `this.props`, we can use it in our component - note that it is passed in as a function so we need to call it, not just use the variable/prop name. That way, we will know to output our stuff properly.

Since we're calling a function, we can also pass info/properties back to it! So what we're doing is loading our Toggle component, passing it a function as a prop (`render`), then within the component we're calling that function and passing it in some data. Within our Toggle component, we also have access to out `toggle` method and the component `state`. So we can pass _that_ info in as an object when calling the `render()` function as well. Then, when rendering the component with the `render` prop, we can destructure the object properties that are passed in. Then we can use those properties in the `render` prop of the component for output.

```
/* From App.js */
<Toggle
	render={ ( { on, toggle } ) => (
		// above, we destructure `on` and `toggle` from the object passed in
		<div>
			{ on && <h1>Hello!</h1> }
			<button onClick={ toggle }>Show / Hide</button>
		</div>
	) }
/>

/* From ToggleRenderProps/index.js */
render() {
	const { render } = this.props;

	return (
		<div>
			{ render( {
				on: this.state.on,
				toggle: this.toggle,
			} ) }
		</div>
	);
}
```

The nice thing about this is that when you reuse the component with the `render` props, the code that is output is completely independent of each other and it's totally reusable.