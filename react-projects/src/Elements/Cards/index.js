/**
 * External Dependencies
 *
 * @format
 */
import styled from 'styled-components';

/**
 * Internal Dependencies
 *
 * @format
 */
//import { elevation, transition, colors } from 'Utilities'; // WOuld need to access as `colors.black`
import { elevation, transition, black } from 'Utilities';

export const Card = styled.div`
	background: white;
	border-radius: 5px;
	color: ${ black };
	${ elevation[ 2 ] };
	margin: 0 auto;
	max-width: 320px;
	padding: 20px;
	${ transition( { property: 'box-shadow' } ) };

	&:hover {
		${ elevation[ 4 ] };
	}
`;
