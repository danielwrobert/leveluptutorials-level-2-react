## Lesson 3: Children Render Props

Here we'll be using child props to clean things up a bit more.

As mentioned in the previous lesson, `render` is not a special name. In this lesson, however, we will be rewriting our render prop with a _special_ prop name.

The _special_ prop name is the `children` prop. This prop is automatically passed in to a component where there is data in between the opening and closing tag (like you see in the first Toggle component via `this.props.children`. We can take advantage of this and not have to explicitly create a prop on our component, rather we can just pass out info in between the opening/closing tags in our `App.js` file. This is a common pattern that you may have seen before - or will likely see at some point.

In order to make this work and to be able to call the `children` prop as a function in our Toggle component, we need to pass a function _as_ the children of the Toggle component, in between the opening/closing tags (otherwise you will get an error stating that `children` is not a function):

```
<Toggle>
	{ ( { on, toggle } ) => (
		// above, we destructure `on` and `toggle` from the object passed in
		<div>
			{ on && <h1>Hello!</h1> }
			<button onClick={ toggle }>Show / Hide</button>
		</div>
	) }
</Toggle>
```

In the ToggleRPC index file (Toggle Render Props Children), we can update the destructuring and call to the previous `render` function to use `children` instead. So now, in our `render` method, we're just going to return the results of the function call from `children`.

```
render() {
	const { children } = this.props;

	return children( {
		on: this.state.on,
		toggle: this.toggle,
	} );
}
```

So now we have a Toggle component which gets passed a function as the `children` which accepts `on` and `toggle` params that are then used in correlation with the state in the Toggle component to manage the functionality.

This gives us a lot more functionality where the Toggle is not tied to anything, we're just passing a function and getting a boolean value from the Toggle component's state. All of the markup is getting passed into the function that is getting passed as `children`. That's why we're just passing the function results of the `children` call from the Toggle component - which is just the state connection. You could even pass in another component and pass along the `on` value to that component as a prop, if you wanted to (instead of the `<h1>Hello!</h1>` above).