/**
 * Exernal Dependencies
 *
 * @format
 */
import React, { Component } from 'react';
import { Gesture } from 'react-with-gesture';
import { Spring, animated, interpolate } from 'react-spring';
import styled from 'styled-components';

/**
 * Inernal Dependencies
 */
import { Card } from '../Cards';

const AnimCard = Card.withComponent( animated.div );

const DragCard = AnimCard.extend`
	box-sizing: border-box;
	height: 300px;
	/* position: absolute;
	width: 100%; */
`;

const CardContainer = styled( animated.div )`
	background: #ccc;
	border-radius: 5px;
	/* position: relative; */
	max-width: 320px;
	/* margin: 0 auto; */
	width: 100vw;
	position: absolute;
	transform: translateX(-50%);
	left: 50%;
`;

export default class Drag extends Component {
	onUp = xDelta => () => {
		console.log( xDelta );
		if ( xDelta < -300 ) {
			alert( 'Remove Card!' );
		} else if ( xDelta > 300 ) {
			alert( 'Accept Card!' );
		}
	};

	render() {
		return (
			<Gesture>
				{ ( { down, xDelta } ) => (
					// { ( { down, x, y, xDelta, yDelta, xInitial, yInitial } ) => (
					// <Card
					// 	style={ {
					// 		transform: `translate3d(${ xDelta }px, ${ yDelta }px, 0)`,
					// 	} }
					// >

					<Spring
						native
						to={ {
							x: down ? xDelta : 0, // this will produce an x value in a render prop
						} }
						immediate={ name => down && name === 'x' }
					>
						{ /* immediate (above) returns true if name of value changing is 'x' and the mouse is down */ }
						{ /* the x in the render prop below comes from the value in our `to` prop above */ }
						{ ( { x } ) => (
							<CardContainer
								style={ {
									background: x.interpolate( {
										range: [ -300, 300 ],
										output: [ '#ff1c68', '#14d790' ],
										extrapolate: 'clamp',
									} ),
								} }
							>
								<DragCard
									onMouseUp={ () => this.onUp( xDelta ) }
									onTouchEnd={ () => this.onUp( xDelta ) }
									style={ {
										opacity: x.interpolate( {
											range: [ -300, -200 ],
											output: [ 0.2, 1 ],
											extrapolate: 'clamp',
										} ),
										transform: interpolate(
											[
												x,
												x.interpolate( {
													range: [ -300, 300 ],
													output: [ -45, 45 ],
													extrapolate: 'clamp',
												} ),
											],
											( x, rotate ) => `translateX(${ x }px) rotate(${ rotate }deg)`
										),
									} }
								>
									<h1>Drag me!</h1>
									<p>{ xDelta }</p>
								</DragCard>
							</CardContainer>
						) }
					</Spring>
				) }
			</Gesture>
		);
	}
}
